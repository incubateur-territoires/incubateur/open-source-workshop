---
theme: default
titleTemplate: Atelier Open Source -- NEC 2022
class: 'text-center'
css: unocss

---

## NEC 2022
# Atelier Open Source

Présenté par V.Agnano et C.Capelli

<div class="abs-br m-6 flex gap-2">
<img src="/logo_nec.png" style="width: 10em">
<img src="/logo_anct.svg" style="width: 10em">

</div>

---
layout: intro
---

# **Open Source**
## Qu'est ce que ça vous évoque ?

---
layout: intro
---

# En croise-t-on au **quotidien** ?

<p>
    <span v-click>Tous les téléphones sous <strong>Android</strong></span>,
    <span v-click>la totalité des <strong>systèmes Apple</strong></span>,
    <span v-click>les systèmes embarqués <strong>dans les voitures</strong></span>,
    <span v-click><strong>Internet</strong></span>,
    <span v-click>l'encodage du streaming video <strong>futur</strong></span>,
    <span v-click><strong>les supercalculateurs</strong></span>,
    <span v-click>etc.</span>
</p>

---
layout: intro
---

# **Quels préjugés** ?

<ul>
    <li v-click>
        Je rends mon code public, n'importe quel hacker pourrait l'exploiter pour me nuire
    </li>
    <li v-click>
        Je prends le risque de me faire voler mon travail
    </li>
    <li v-click>
        Mon service n'est pas terminé, je ne vais pas l'ouvrir tout de suite
    </li>
    <li v-click>
        La maintenance va me couter du temps et des ressources techniques
    </li>
</ul>

<br>

<p style="font-size: 2em" v-click>Au final, je n'y gagne... <strong>RIEN</strong></p>

---
layout: intro
---

# Comment fait-on pour ouvrir son code ?
<ul>
    <li v-click>
        Héberger son code en ligne
    </li>
    <li v-click>
        Mettre en place un workflow de contribution
    </li>
    <li v-click>
        Choisir une licence
    </li>
</ul>

---

<img src="/licence_libre.png" style="height: 100%; margin: auto">

---

# Merci !

Retrouvez tout le contenu de cette présentation sur
<br>
*https://gitlab.com/incubateur-territoires/incubateur/open-source-workshop*

## Plus d'infos ? **Contactez nous** !

charles.capelli@qonfucius.team,
vincent.agnano@anct.gouv.fr

---
